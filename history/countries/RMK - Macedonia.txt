government = feudal_monarchy
government_rank = 3
mercantilism = 2
technology_group = eastern
religion = orthodox
primary_culture = greek
capital = 3001	# Skopje

1399.1.1 = {
	monarch = {
		name = "Alexander II"
		adm = 1
		dip = 3
		mil = 3
		regent = yes
	}
}
