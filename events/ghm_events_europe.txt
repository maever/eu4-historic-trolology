namespace = ghm_events

# The Surrender of Gibraltar
country_event = {
	id = ghm_events.1
	country = GBR
	title = "ghm_events.1TITLE"
	desc = "ghm_events.1DESC"
	picture = SIEGE_eventPicture
	major = yes
	
	fire_only_once = yes
	
	trigger = {
		tag = GBR
		NOT = { war_with = SPA }
		226 = {
			owned_by = SPA
		}
	}
	
	mean_time_to_happen = {
		months = 12
	}

	option = {
		name = "ghm_events.1ACTION"
		ai_chance = {
			factor = 90
		}
		tooltip = {
			226 = {
				cede_province = GBR
			}
			add_truce_with = SPA
		}
		add_prestige = 5
	}
	option = {
		ai_chance = {
			factor = 10
		}
        name = "ghm_events.1ACTION_ALT"
    }
}

# Calais Refugee Camp
country_event = {
	id = ghm_events.2
	country = FRA
	title = "ghm_events.2TITLE"
	desc = "ghm_events.2DESC"
	picture = ANGRY_MOB_eventPicture
	major = yes
	
	fire_only_once = no
	
	trigger = {
		tag = FRA
		AND = {
		tag = TUR
			is_at_war
		}
	}
	
	mean_time_to_happen = {
		months = 4
	}

	option = {
		name = "ghm_events.2ACTION"
		ai_chance = {
			factor = 50
		}
		add_prestige = -5
		add_local_autonomy = 25
	}
	option = {
        name = "ghm_events.2ACTION_ALT"
		ai_chance = {
			factor = 50
		}
		add_prestige = -5
	}
}

# BREXIT
country_event = {
	id = ghm_events.3
	country = GBR
	title = "ghm_events.3TITLE"
	desc = "ghm_events.3DESC"
	picture = ANGRY_MOB_eventPicture
	major = yes
	
	fire_only_once = yes
	
	trigger = {
		tag = GBR
		has_any_disaster = no 
	}
	
	mean_time_to_happen = {
		months = 48
	}

	immediate = {
		random_owned_province = {
			limit = {
				is_core = ROOT 
				development = 10
				is_capital = no
			}
			spawn_rebels = {
				type = 	pretender_rebels
				size = 2
				win = yes
			}
			add_local_autonomy = 50
		}
	}

	option = {
		name = "ghm_events.3ACTION"
		ai_chance = {
			factor = 100
		}
		add_prestige = -50
	}
}